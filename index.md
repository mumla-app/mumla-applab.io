---
title: Mumla
lang: en
---

::: center

![](icon.png "Mumla icon")

# Mumla

[Mumla needs a new maintainer](https://gitlab.com/quite/mumla#software-maintenance-situation)

[Mumble](https://mumble.info/) app for Android -- [Copyleft](https://en.wikipedia.org/wiki/Copyleft) & [Free/libre](https://en.wikipedia.org/wiki/Free_software)

Mumble is a low latency, high quality voice conference and chat system. Connect
to one of the many public Mumble servers, or [run a server](https://wiki.mumble.info/wiki/Murmurguide)
for your own community!

[![Get it on F-Droid](get-it-on.png)](https://f-droid.org/packages/se.lublin.mumla/ "Get it on F-Droid")
[![Get it on Goog playstore](get-it-on-gp.png)](https://play.google.com/store/apps/details?id=se.lublin.mumla.donation "Get it on Gogl play (donation version)")

Donate through [Liberapay](https://liberapay.com/quite/donate), [Paypal](https://paypal.me/dlublin).

Help translating [on Weblate](https://hosted.weblate.org/engage/mumla/).

___

[source code](https://gitlab.com/quite/mumla) | [issues](https://gitlab.com/quite/mumla/-/issues)

[site repo](https://gitlab.com/mumla-app/mumla-app.gitlab.io)

:::
