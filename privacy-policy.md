---
title: Mumla -- Privacy Policy
lang: en
---

# Privacy Policy

The Mumla Android application ("Mumla") built by Daniel Lublin is released
under a Free/Libre Open Source license. Mumla is available at no cost,
donations are optional.

## Collection, Use, and Sharing of Personal Information

Mumla can record audio from a microphone on the device that it is running on.

Audio may be recorded after the user first instructs Mumla to connect to a
Mumble[^mumble] (or compatible) server (the "server") of the user's choosing,
typically operated by a third party. The server may enforce its own privacy
policies, distinct from that of Mumla. The user is advised to connect only to
trusted servers.

Depending on how Mumla is set up, audio recording may be carried out in either
of the two following situations:

  1. on the user's initiative (when tapping or pressing a button), or
  2. by voice activation (if speech or other sound reaches a sufficient
     volume).

The recorded audio is immediately transmitted by Mumla to the server of choice.
The purpose of which is to facilitate dialogue between the user of Mumla, and
peers connected to the server.

Mumla does not store recorded audio on the device, or anywhere else.

## Changes to the Privacy Policy

Any updates to the Privacy Policy will be made available on this page, along
with details on what changed.

This policy is effective as of 2020-05-13 and has not been updated since.

## Contact

If you have any questions regarding this policy, contact mumla@lublin.se.

[^mumble]: <https://www.mumble.info/>
