---
title: Mumla -- Beta releases
lang: en
---

# Beta releases

Beta builds of Mumla (that installs side-by-side with the F-Droid release) are
available in [a separate F-Droid repository](fdroidrepos://lublin.se/fdroid/repo?fingerprint=4FE75AB58C310E9778FBA716A0D1D66E8F49F697737BC0EE2437AEAE278CF64C).

That's an fdroidrepos-URI, to be followed on a device with F-Droid installed.
Or scan this QR code:

![](https://mumla-app.gitlab.io/lublin-fdroid-repo-qrcode.png)

[back to main page](../)
